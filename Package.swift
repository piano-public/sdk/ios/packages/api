// swift-tools-version:5.7

import PackageDescription

let package = Package(
    name: "PianoAPI",
    platforms: [
        .iOS(.v9)
    ],
    products: [
        .library(
            name: "PianoAPI",
            targets: ["PianoAPI"]
        )
    ],
    dependencies: [],
    targets: [
        .target(
            name: "PianoAPI",
            dependencies: [],
            path: "Sources"
        ),
    ]
)
