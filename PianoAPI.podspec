Pod::Spec.new do |s|
    s.name         = 'PianoAPI'
    s.version      = '1.0.2'
    s.swift_version = '5.0'
    s.summary      = 'Piano API for iOS'
    s.homepage     = 'https://gitlab.com/piano-public/sdk/ios/packages/api'
    s.license      = { :type => 'Apache License, Version 2.0', :file => 'LICENSE' }
    s.author       = 'Piano Inc.'
    s.platform     = :ios, '9.0'
    s.source       = { :git => 'https://gitlab.com/piano-public/sdk/ios/packages/api.git', :tag => "#{s.version}" }
    s.source_files = 'Sources/**/*.swift', 'Sources/**/*.h'
    s.static_framework = true
end
