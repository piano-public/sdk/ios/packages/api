import Foundation

@objc(PianoAPIPasswordlessPurchaseCompleteResult)
public class PasswordlessPurchaseCompleteResult: NSObject, Codable {

    /// The order ID
    @objc public var oid: String? = nil

    /// The URL of the page
    @objc public var url: String? = nil

    /// The resource
    @objc public var resource: Resource? = nil

    /// Parameters to show offer for continue payment flow after confirmation
    @objc public var showOfferParams: String? = nil

    /// The term type
    @objc public var type: String? = nil

    /// Process ID
    @objc public var processId: String? = nil

    /// Is polling enabled
    @objc public var pollingEnabled: OptionalBool? = nil

    
    @objc public var pollingTimeouts: [OptionalInt]? = nil

    public enum CodingKeys: String, CodingKey {
        case oid = "oid"
        case url = "url"
        case resource = "resource"
        case showOfferParams = "show_offer_params"
        case type = "type"
        case processId = "process_id"
        case pollingEnabled = "polling_enabled"
        case pollingTimeouts = "polling_timeouts"
    }
}
