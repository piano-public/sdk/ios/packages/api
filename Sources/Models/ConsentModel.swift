import Foundation

@objc(PianoAPIConsentModel)
public class ConsentModel: NSObject, Codable {

    /// The displayed text of the consent box
    @objc public var consentPubId: String? = nil

    /// The displayed text of the consent box
    @objc public var displayText: String? = nil

    /// The name of the consent box field
    @objc public var fieldName: String? = nil

    /// The ID of the consent box field
    @objc public var fieldId: String? = nil

    /// Consent sort order
    @objc public var sortOrder: String? = nil

    /// Whether the consent box is checked
    @objc public var checked: String? = nil

    public enum CodingKeys: String, CodingKey {
        case consentPubId = "consent_pub_id"
        case displayText = "display_text"
        case fieldName = "field_name"
        case fieldId = "field_id"
        case sortOrder = "sort_order"
        case checked = "checked"
    }
}
