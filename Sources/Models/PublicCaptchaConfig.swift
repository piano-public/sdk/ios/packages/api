import Foundation

@objc(PianoAPIPublicCaptchaConfig)
public class PublicCaptchaConfig: NSObject, Codable {

    /// The application ID
    @objc public var aid: String? = nil

    /// Whether the property is enabled
    @objc public var enabled: OptionalBool? = nil

    /// reCAPTCHA v3 site key
    @objc public var captcha3SiteKey: String? = nil

    public enum CodingKeys: String, CodingKey {
        case aid = "aid"
        case enabled = "enabled"
        case captcha3SiteKey = "captcha3_site_key"
    }
}
