import Foundation

@objc(PianoAPITemplateContext)
public class TemplateContext: NSObject, Codable {

    /// The user info
    @objc public var userInfo: UserInfo? = nil

    /// The experience ID
    @objc public var experienceId: String? = nil

    /// The experience action ID
    @objc public var experienceActionId: String? = nil

    /// The experience execution ID
    @objc public var experienceExecutionId: String? = nil

    /// The resolved template language
    @objc public var templateLanguage: String? = nil

    public enum CodingKeys: String, CodingKey {
        case userInfo = "user_info"
        case experienceId = "experience_id"
        case experienceActionId = "experience_action_id"
        case experienceExecutionId = "experience_execution_id"
        case templateLanguage = "template_language"
    }
}
