import Foundation

@objc(PianoAPILinkedTermCheckoutTrackParams)
public class LinkedTermCheckoutTrackParams: NSObject, Codable {

    /// The application ID
    @objc public var aid: String? = nil

    /// The unique browser ID
    @objc public var browserId: String? = nil

    /// The ID of the term in the external system. Provided by the external system.
    @objc public var externalTermId: String? = nil

    /// The Unix timestamp of when user started an external checkout
    @objc public var checkoutStartDate: OptionalInt64? = nil

    public enum CodingKeys: String, CodingKey {
        case aid = "aid"
        case browserId = "browser_id"
        case externalTermId = "external_term_id"
        case checkoutStartDate = "checkout_start_date"
    }
}
