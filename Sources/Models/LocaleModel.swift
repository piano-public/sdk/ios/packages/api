import Foundation

@objc(PianoAPILocaleModel)
public class LocaleModel: NSObject, Codable {

    /// Locale
    @objc public var locale: String? = nil

    /// Label
    @objc public var label: String? = nil

    /// Localized label
    @objc public var localizedLabel: String? = nil

    /// Is default locale
    @objc public var isDefault: OptionalBool? = nil

    /// Is enabled
    @objc public var isEnabled: OptionalBool? = nil

    /// Is RTL
    @objc public var isRtl: OptionalBool? = nil

    public enum CodingKeys: String, CodingKey {
        case locale = "locale"
        case label = "label"
        case localizedLabel = "localized_label"
        case isDefault = "is_default"
        case isEnabled = "is_enabled"
        case isRtl = "is_rtl"
    }
}
