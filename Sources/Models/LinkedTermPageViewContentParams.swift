import Foundation

@objc(PianoAPILinkedTermPageViewContentParams)
public class LinkedTermPageViewContentParams: NSObject, Codable {

    /// When the content was published
    @objc public var contentCreated: String? = nil

    /// The author of the content
    @objc public var contentAuthor: String? = nil

    /// The section for the content
    @objc public var contentSection: String? = nil

    /// The type of the content
    @objc public var contentType: String? = nil

    /// The tags of the page
    @objc public var tags: [String]? = nil

    public enum CodingKeys: String, CodingKey {
        case contentCreated = "content_created"
        case contentAuthor = "content_author"
        case contentSection = "content_section"
        case contentType = "content_type"
        case tags = "tags"
    }
}
