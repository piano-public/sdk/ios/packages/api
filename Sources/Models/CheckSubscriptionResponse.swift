import Foundation

@objc(PianoAPICheckSubscriptionResponse)
public class CheckSubscriptionResponse: NSObject, Codable {

    /// Type
    @objc public var type: String? = nil

    /// User token
    @objc public var userToken: String? = nil

    /// Term id
    @objc public var termId: String? = nil

    public enum CodingKeys: String, CodingKey {
        case type = "type"
        case userToken = "user_token"
        case termId = "term_id"
    }
}
