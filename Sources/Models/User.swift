import Foundation

@objc(PianoAPIUser)
public class User: NSObject, Codable {

    /// The user&#39;s ID
    @objc public var uid: String? = nil

    /// The user&#39;s email address (single)
    @objc public var email: String? = nil

    /// The user&#39;s first name
    @objc public var firstName: String? = nil

    /// The user&#39;s last name
    @objc public var lastName: String? = nil

    /// The user&#39;s personal name. Name and surname ordered as per locale
    @objc public var personalName: String? = nil

    /// The user creation date
    @objc public var createDate: Date? = nil

    public enum CodingKeys: String, CodingKey {
        case uid = "uid"
        case email = "email"
        case firstName = "first_name"
        case lastName = "last_name"
        case personalName = "personal_name"
        case createDate = "create_date"
    }
}
