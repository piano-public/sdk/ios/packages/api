import Foundation

@objc(PianoAPIResource)
public class Resource: NSObject, Codable {

    /// The resource ID
    @objc public var rid: String? = nil

    /// The application ID
    @objc public var aid: String? = nil

    /// The name
    @objc public var name: String? = nil

    /// The resource description
    @objc public var _description: String? = nil

    /// The URL of the resource image
    @objc public var imageUrl: String? = nil

    public enum CodingKeys: String, CodingKey {
        case rid = "rid"
        case aid = "aid"
        case name = "name"
        case _description = "description"
        case imageUrl = "image_url"
    }
}
