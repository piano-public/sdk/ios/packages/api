import Foundation

@objc(PianoAPICurrency)
public class Currency: NSObject, Codable {

    /// The currency code under ISO 4217
    @objc public var currencyCode: String? = nil

    /// The currency symbol
    @objc public var currencySymbol: String? = nil

    public enum CodingKeys: String, CodingKey {
        case currencyCode = "currency_code"
        case currencySymbol = "currency_symbol"
    }
}
