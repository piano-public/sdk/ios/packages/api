import Foundation

@objc(PianoAPIUserInfo)
public class UserInfo: NSObject, Codable {

    /// The user&#39;s language
    @objc public var userLang: String? = nil

    /// Whether the user is a new customer
    @objc public var isNewCustomer: OptionalBool? = nil

    /// The user
    @objc public var user: UserModel? = nil

    /// The country code
    @objc public var countryCode: String? = nil

    /// The user&#39;s postal code
    @objc public var postalCode: String? = nil

    /// Caption of tax support
    @objc public var taxSupport: String? = nil

    public enum CodingKeys: String, CodingKey {
        case userLang = "user_lang"
        case isNewCustomer = "is_new_customer"
        case user = "user"
        case countryCode = "country_code"
        case postalCode = "postal_code"
        case taxSupport = "tax_support"
    }
}
