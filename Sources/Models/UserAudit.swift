import Foundation

@objc(PianoAPIUserAudit)
public class UserAudit: NSObject, Codable {

    /// uid
    @objc public var uid: String? = nil

    /// An audit action type, such as: login, profile update, password reset
    @objc public var actionType: String? = nil

    /// The application ID
    @objc public var aid: String? = nil

    public enum CodingKeys: String, CodingKey {
        case uid = "uid"
        case actionType = "action_type"
        case aid = "aid"
    }
}
