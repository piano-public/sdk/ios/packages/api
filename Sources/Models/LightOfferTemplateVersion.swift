import Foundation

@objc(PianoAPILightOfferTemplateVersion)
public class LightOfferTemplateVersion: NSObject, Codable {

    /// The template ID
    @objc public var offerTemplateId: String? = nil

    /// Is template token type
    @objc public var tokenType: String? = nil

    /// The version
    @objc public var version: OptionalInt? = nil

    /// The type 
    @objc public var type: String? = nil

    /// HTML
    @objc public var content1Type: String? = nil

    /// CSS
    @objc public var content2Type: String? = nil

    /// The content1 value
    @objc public var content1Value: String? = nil

    /// The content2 value
    @objc public var content2Value: String? = nil

    /// The template ID
    @objc public var templateId: String? = nil

    /// The template version ID
    @objc public var templateVersionId: String? = nil

    /// Whether the template is archived
    @objc public var isOfferTemplateArchived: OptionalBool? = nil

    /// Whether the template variant is archived
    @objc public var isTemplateVariantArchived: OptionalBool? = nil

    
    @objc public var externalCssList: [ExternalCss]? = nil

    public enum CodingKeys: String, CodingKey {
        case offerTemplateId = "offer_template_id"
        case tokenType = "token_type"
        case version = "version"
        case type = "type"
        case content1Type = "content1_type"
        case content2Type = "content2_type"
        case content1Value = "content1_value"
        case content2Value = "content2_value"
        case templateId = "template_id"
        case templateVersionId = "template_version_id"
        case isOfferTemplateArchived = "is_offer_template_archived"
        case isTemplateVariantArchived = "is_template_variant_archived"
        case externalCssList = "external_css_list"
    }
}
