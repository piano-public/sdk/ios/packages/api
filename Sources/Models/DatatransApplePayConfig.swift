import Foundation

@objc(PianoAPIDatatransApplePayConfig)
public class DatatransApplePayConfig: NSObject, Codable {

    /// The URL of the page
    @objc public var url: String? = nil

    public enum CodingKeys: String, CodingKey {
        case url = "url"
    }
}
