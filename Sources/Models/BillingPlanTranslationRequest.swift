import Foundation

@objc(PianoAPIBillingPlanTranslationRequest)
public class BillingPlanTranslationRequest: NSObject, Codable {

    /// The billing plan of the subscription
    @objc public var billingPlan: String? = nil

    /// The date of the last payment for the subscription
    @objc public var subscriptionLastPayment: Date? = nil

    /// The start date.
    @objc public var startDate: Date? = nil

    /// The end date
    @objc public var endDate: Date? = nil

    /// The public ID of the term
    @objc public var termPubId: String? = nil

    public enum CodingKeys: String, CodingKey {
        case billingPlan = "billing_plan"
        case subscriptionLastPayment = "subscription_last_payment"
        case startDate = "start_date"
        case endDate = "end_date"
        case termPubId = "term_pub_id"
    }
}
