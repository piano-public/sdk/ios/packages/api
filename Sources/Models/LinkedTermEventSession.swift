import Foundation

@objc(PianoAPILinkedTermEventSession)
public class LinkedTermEventSession: NSObject, Codable {

    
    @objc public var trackingId: String? = nil

    
    @objc public var tbc: String? = nil

    
    @objc public var pcid: String? = nil

    
    @objc public var offer: LinkedTermEventSessionOffer? = nil

    
    @objc public var pageViewId: String? = nil

    
    @objc public var pageViewContent: LinkedTermEventPageViewContent? = nil

    
    @objc public var userAgent: String? = nil

    
    @objc public var consents: String? = nil

    
    @objc public var previousUserSegments: String? = nil

    public enum CodingKeys: String, CodingKey {
        case trackingId = "trackingId"
        case tbc = "tbc"
        case pcid = "pcid"
        case offer = "offer"
        case pageViewId = "pageViewId"
        case pageViewContent = "pageViewContent"
        case userAgent = "userAgent"
        case consents = "consents"
        case previousUserSegments = "previousUserSegments"
    }
}
