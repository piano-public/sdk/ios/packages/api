import Foundation

@objc(PianoAPILinkedTermEventPageViewContent)
public class LinkedTermEventPageViewContent: NSObject, Codable {

    
    @objc public var contentCreated: String? = nil

    
    @objc public var contentAuthor: String? = nil

    
    @objc public var contentSection: String? = nil

    
    @objc public var contentType: String? = nil

    
    @objc public var tags: [String]? = nil

    public enum CodingKeys: String, CodingKey {
        case contentCreated = "contentCreated"
        case contentAuthor = "contentAuthor"
        case contentSection = "contentSection"
        case contentType = "contentType"
        case tags = "tags"
    }
}
