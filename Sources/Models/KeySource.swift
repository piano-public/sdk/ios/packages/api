import Foundation

@objc(PianoAPIKeySource)
public class KeySource: NSObject, Codable {

    /// Message key
    @objc public var key: String? = nil

    
    @objc public var keySources: [Source]? = nil

    public enum CodingKeys: String, CodingKey {
        case key = "key"
        case keySources = "key_sources"
    }
}
