import Foundation

@objc(PianoAPISwgResponse)
public class SwgResponse: NSObject, Codable {

    /// The subscription token
    @objc public var subscriptionToken: String? = nil

    /// Detail
    @objc public var detail: String? = nil

    
    @objc public var products: [String]? = nil

    public enum CodingKeys: String, CodingKey {
        case subscriptionToken = "subscriptionToken"
        case detail = "detail"
        case products = "products"
    }
}
