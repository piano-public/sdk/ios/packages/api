import Foundation

@objc(PianoAPIExternalCss)
public class ExternalCss: NSObject, Codable {

    /// The title
    @objc public var title: String? = nil

    /// The URL of the page
    @objc public var url: String? = nil

    /// The status of the external CSS of the template(\&quot;active\&quot; or \&quot;inactive\&quot;)
    @objc public var status: String? = nil

    /// The position
    @objc public var position: OptionalInt? = nil

    /// The external CSS ID
    @objc public var externalCssId: String? = nil

    public enum CodingKeys: String, CodingKey {
        case title = "title"
        case url = "url"
        case status = "status"
        case position = "position"
        case externalCssId = "external_css_id"
    }
}
