import Foundation

@objc(PianoAPIRegion)
public class Region: NSObject, Codable {

    /// The name of the country region
    @objc public var regionName: String? = nil

    /// The code of the country region
    @objc public var regionCode: String? = nil

    /// The ID of the country region
    @objc public var regionId: String? = nil

    public enum CodingKeys: String, CodingKey {
        case regionName = "region_name"
        case regionCode = "region_code"
        case regionId = "region_id"
    }
}
