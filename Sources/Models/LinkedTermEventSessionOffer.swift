import Foundation

@objc(PianoAPILinkedTermEventSessionOffer)
public class LinkedTermEventSessionOffer: NSObject, Codable {

    
    @objc public var offerId: String? = nil

    
    @objc public var offerTemplateId: String? = nil

    
    @objc public var offerTemplateVersionId: String? = nil

    public enum CodingKeys: String, CodingKey {
        case offerId = "offerId"
        case offerTemplateId = "offerTemplateId"
        case offerTemplateVersionId = "offerTemplateVersionId"
    }
}
