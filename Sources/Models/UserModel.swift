import Foundation

@objc(PianoAPIUserModel)
public class UserModel: NSObject, Codable {

    /// The user&#39;s ID
    @objc public var uid: String? = nil

    /// The user&#39;s first name
    @objc public var firstName: String? = nil

    /// The user&#39;s last name
    @objc public var lastName: String? = nil

    /// The user&#39;s email address (single)
    @objc public var email: String? = nil

    /// valid
    @objc public var valid: OptionalBool? = nil

    public enum CodingKeys: String, CodingKey {
        case uid = "uid"
        case firstName = "first_name"
        case lastName = "last_name"
        case email = "email"
        case valid = "valid"
    }
}
