import Foundation

@objc(PianoAPIPasswordlessPurchaseCheckResult)
public class PasswordlessPurchaseCheckResult: NSObject, Codable {

    /// Polling status
    @objc public var pollStatus: String? = nil

    /// The resource
    @objc public var resource: Resource? = nil

    /// Parameters to show offer for continue payment flow after confirmation
    @objc public var showOfferParams: String? = nil

    /// The term type
    @objc public var type: String? = nil

    public enum CodingKeys: String, CodingKey {
        case pollStatus = "poll_status"
        case resource = "resource"
        case showOfferParams = "show_offer_params"
        case type = "type"
    }
}
