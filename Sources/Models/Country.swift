import Foundation

@objc(PianoAPICountry)
public class Country: NSObject, Codable {

    /// The country name
    @objc public var countryName: String? = nil

    /// The country code
    @objc public var countryCode: String? = nil

    /// The country ID
    @objc public var countryId: String? = nil

    
    @objc public var regions: [Region]? = nil

    public enum CodingKeys: String, CodingKey {
        case countryName = "country_name"
        case countryCode = "country_code"
        case countryId = "country_id"
        case regions = "regions"
    }
}
