import Foundation

@objc(PianoAPISource)
public class Source: NSObject, Codable {

    /// Message context
    @objc public var type: String? = nil

    
    @objc public var sourceNames: [String]? = nil

    public enum CodingKeys: String, CodingKey {
        case type = "type"
        case sourceNames = "source_names"
    }
}
