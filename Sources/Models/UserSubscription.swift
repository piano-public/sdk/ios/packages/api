import Foundation

@objc(PianoAPIUserSubscription)
public class UserSubscription: NSObject, Codable {

    /// The user subscription ID
    @objc public var subscriptionId: String? = nil

    
    @objc public var term: Term? = nil

    /// Whether auto renewal is enabled for the subscription
    @objc public var autoRenew: OptionalBool? = nil

    /// The grace period start date
    @objc public var gracePeriodStartDate: Date? = nil

    /// The next bill date of the subscription
    @objc public var nextBillDate: Date? = nil

    /// The start date.
    @objc public var startDate: Date? = nil

    /// The subscription status
    @objc public var status: String? = nil

    /// Whether this subscription can be cancelled; cancelling means thatthe access will not be prolonged and the current access will be revoked
    @objc public var cancelable: OptionalBool? = nil

    /// Whether the subscription can be cancelled with the payment for the last period refunded. Cancelling means that the access will not be prolonged and the current access will be revoked
    @objc public var cancelableAndRefundadle: OptionalBool? = nil

    /// The description of the term billing plan
    @objc public var paymentBillingPlanDescription: String? = nil

    public enum CodingKeys: String, CodingKey {
        case subscriptionId = "subscription_id"
        case term = "term"
        case autoRenew = "auto_renew"
        case gracePeriodStartDate = "grace_period_start_date"
        case nextBillDate = "next_bill_date"
        case startDate = "start_date"
        case status = "status"
        case cancelable = "cancelable"
        case cancelableAndRefundadle = "cancelable_and_refundadle"
        case paymentBillingPlanDescription = "payment_billing_plan_description"
    }
}
