import Foundation

@objc(PianoAPILinkedTermSessionParams)
public class LinkedTermSessionParams: NSObject, Codable {

    /// The User&#39;s tracking id
    @objc public var trackingId: String? = nil

    /// The User&#39;s tbc
    @objc public var tbc: String? = nil

    /// The User&#39;s pcid
    @objc public var pcid: String? = nil

    /// The offer ID
    @objc public var offerId: String? = nil

    /// The template ID
    @objc public var offerTemplateId: String? = nil

    /// The ID of the template version ID
    @objc public var offerTemplateVersionId: String? = nil

    /// The pageview ID, can be retrieved from cookies through JS SDK
    @objc public var pageViewId: String? = nil

    /// PageView content parameters
    @objc public var pageViewContent: LinkedTermPageViewContentParams? = nil

    /// The conversion consents in JSON format
    @objc public var consents: String? = nil

    /// Previous C1X user segments
    @objc public var previousUserSegments: String? = nil

    public enum CodingKeys: String, CodingKey {
        case trackingId = "tracking_id"
        case tbc = "tbc"
        case pcid = "pcid"
        case offerId = "offer_id"
        case offerTemplateId = "offer_template_id"
        case offerTemplateVersionId = "offer_template_version_id"
        case pageViewId = "page_view_id"
        case pageViewContent = "page_view_content"
        case consents = "consents"
        case previousUserSegments = "previous_user_segments"
    }
}
