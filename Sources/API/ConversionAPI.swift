import Foundation

@objc(PianoAPIConversionAPI)
public class ConversionAPI: NSObject {

    /// List conversions (anon)
    /// - Parameters:
    ///   - aid: The application ID
    ///   - offset: Offset from which to start returning results
    ///   - limit: Maximum index of returned results
    ///   - userToken: The user token
    ///   - userProvider: The user token provider
    ///   - userRef: The encrypted user reference
    ///   - callback: Operation callback
    @objc public func list(
        aid: String,
        offset: OptionalInt = 0,
        limit: OptionalInt = 100,
        userToken: String? = nil,
        userProvider: String? = nil,
        userRef: String? = nil,
        completion: @escaping ([TermConversion]?, Error?) -> Void) {
        guard let client = PianoAPI.shared.client else {
            completion(nil, PianoAPIError("PianoAPI not initialized"))
            return
        }

        var params = [String:String]()
        params["aid"] = aid
        params["offset"] = String(offset.value)
        params["limit"] = String(limit.value)
        if let v = userToken { params["user_token"] = v }
        if let v = userProvider { params["user_provider"] = v }
        if let v = userRef { params["user_ref"] = v }
        client.request(
            path: "/api/v3/conversion/list",
            method: PianoAPI.HttpMethod.from("GET"),
            params: params,
            completion: completion
        )
    }

    /// Log third-party conversion (anon)
    /// - Parameters:
    ///   - trackingId: The conversion ID to track in external systems
    ///   - termId: The term ID
    ///   - termName: The term name
    ///   - stepNumber: The step of the external checkout as defined by the client
    ///   - conversionCategory: The conversion category
    ///   - amount: The conversion amount
    ///   - currency: The conversion currency under the ISO 4217 standard
    ///   - customParams: The custom parameters (any key-value pairs) to save; the value should be a valid JSON object)
    ///   - browserId: The unique browser ID
    ///   - pageTitle: Page title
    ///   - url: The URL of the page
    ///   - referer: The page referer
    ///   - contentAuthor: The author of the content
    ///   - contentCreated: When the content was published
    ///   - contentSection: The section for the content
    ///   - contentType: The type of the content
    ///   - tags: The tags of the page
    ///   - previousUserSegments: Previous C1X user segments
    ///   - cookieConsents: The conversion consents in JSON format
    ///   - callback: Operation callback
    @objc public func logConversion(
        trackingId: String,
        termId: String,
        termName: String,
        stepNumber: OptionalInt? = nil,
        conversionCategory: String? = nil,
        amount: OptionalDouble? = nil,
        currency: String? = nil,
        customParams: String? = nil,
        browserId: String? = nil,
        pageTitle: String? = nil,
        url: String? = nil,
        referer: String? = nil,
        contentAuthor: String? = nil,
        contentCreated: String? = nil,
        contentSection: String? = nil,
        contentType: String? = nil,
        tags: [String]? = nil,
        previousUserSegments: String? = nil,
        cookieConsents: String? = nil,
        completion: @escaping (Error?) -> Void) {
        guard let client = PianoAPI.shared.client else {
            completion(PianoAPIError("PianoAPI not initialized"))
            return
        }

        var params = [String:String]()
        params["tracking_id"] = trackingId
        params["term_id"] = termId
        params["term_name"] = termName
        if let v = stepNumber { params["step_number"] = String(v.value) }
        if let v = conversionCategory { params["conversion_category"] = v }
        if let v = amount { params["amount"] = String(v.value) }
        if let v = currency { params["currency"] = v }
        if let v = customParams { params["custom_params"] = v }
        if let v = browserId { params["browser_id"] = v }
        if let v = pageTitle { params["page_title"] = v }
        if let v = url { params["url"] = v }
        if let v = referer { params["referer"] = v }
        if let v = contentAuthor { params["content_author"] = v }
        if let v = contentCreated { params["content_created"] = v }
        if let v = contentSection { params["content_section"] = v }
        if let v = contentType { params["content_type"] = v }
        if let v = tags { params["tags"] = v.joined(separator: ",") }
        if let v = previousUserSegments { params["previous_user_segments"] = v }
        if let v = cookieConsents { params["cookie_consents"] = v }
        client.request(
            path: "/api/v3/conversion/log",
            method: PianoAPI.HttpMethod.from("POST"),
            params: params,
            completion: completion
        )
    }

    /// Log funnel stage
    /// - Parameters:
    ///   - trackingId: The conversion ID to track in external systems
    ///   - stepNumber: The step of the external checkout as defined by the client
    ///   - stepName: The name of the checkout step
    ///   - customParams: The custom parameters (any key-value pairs) to save; the value should be a valid JSON object)
    ///   - browserId: The unique browser ID
    ///   - pageTitle: Page title
    ///   - url: The URL of the page
    ///   - referer: The page referer
    ///   - contentAuthor: The author of the content
    ///   - contentCreated: When the content was published
    ///   - contentSection: The section for the content
    ///   - contentType: The type of the content
    ///   - tags: The tags of the page
    ///   - previousUserSegments: Previous C1X user segments
    ///   - cookieConsents: The conversion consents in JSON format
    ///   - callback: Operation callback
    @objc public func logFunnelStep(
        trackingId: String,
        stepNumber: OptionalInt,
        stepName: String,
        customParams: String? = nil,
        browserId: String? = nil,
        pageTitle: String? = nil,
        url: String? = nil,
        referer: String? = nil,
        contentAuthor: String? = nil,
        contentCreated: String? = nil,
        contentSection: String? = nil,
        contentType: String? = nil,
        tags: [String]? = nil,
        previousUserSegments: String? = nil,
        cookieConsents: String? = nil,
        completion: @escaping (Error?) -> Void) {
        guard let client = PianoAPI.shared.client else {
            completion(PianoAPIError("PianoAPI not initialized"))
            return
        }

        var params = [String:String]()
        params["tracking_id"] = trackingId
        params["step_number"] = String(stepNumber.value)
        params["step_name"] = stepName
        if let v = customParams { params["custom_params"] = v }
        if let v = browserId { params["browser_id"] = v }
        if let v = pageTitle { params["page_title"] = v }
        if let v = url { params["url"] = v }
        if let v = referer { params["referer"] = v }
        if let v = contentAuthor { params["content_author"] = v }
        if let v = contentCreated { params["content_created"] = v }
        if let v = contentSection { params["content_section"] = v }
        if let v = contentType { params["content_type"] = v }
        if let v = tags { params["tags"] = v.joined(separator: ",") }
        if let v = previousUserSegments { params["previous_user_segments"] = v }
        if let v = cookieConsents { params["cookie_consents"] = v }
        client.request(
            path: "/api/v3/conversion/logFunnelStep",
            method: PianoAPI.HttpMethod.from("POST"),
            params: params,
            completion: completion
        )
    }

    /// Log micro conversion
    /// - Parameters:
    ///   - trackingId: The conversion ID to track in external systems
    ///   - eventGroupId: The conversion event group ID
    ///   - customParams: The custom parameters (any key-value pairs) to save; the value should be a valid JSON object)
    ///   - browserId: The unique browser ID
    ///   - pageTitle: Page title
    ///   - url: The URL of the page
    ///   - referer: The page referer
    ///   - contentAuthor: The author of the content
    ///   - contentCreated: When the content was published
    ///   - contentSection: The section for the content
    ///   - contentType: The type of the content
    ///   - tags: The tags of the page
    ///   - previousUserSegments: Previous C1X user segments
    ///   - cookieConsents: The conversion consents in JSON format
    ///   - callback: Operation callback
    @objc public func logMicroConversion(
        trackingId: String,
        eventGroupId: String,
        customParams: String? = nil,
        browserId: String? = nil,
        pageTitle: String? = nil,
        url: String? = nil,
        referer: String? = nil,
        contentAuthor: String? = nil,
        contentCreated: String? = nil,
        contentSection: String? = nil,
        contentType: String? = nil,
        tags: [String]? = nil,
        previousUserSegments: String? = nil,
        cookieConsents: String? = nil,
        completion: @escaping (Error?) -> Void) {
        guard let client = PianoAPI.shared.client else {
            completion(PianoAPIError("PianoAPI not initialized"))
            return
        }

        var params = [String:String]()
        params["tracking_id"] = trackingId
        params["event_group_id"] = eventGroupId
        if let v = customParams { params["custom_params"] = v }
        if let v = browserId { params["browser_id"] = v }
        if let v = pageTitle { params["page_title"] = v }
        if let v = url { params["url"] = v }
        if let v = referer { params["referer"] = v }
        if let v = contentAuthor { params["content_author"] = v }
        if let v = contentCreated { params["content_created"] = v }
        if let v = contentSection { params["content_section"] = v }
        if let v = contentType { params["content_type"] = v }
        if let v = tags { params["tags"] = v.joined(separator: ",") }
        if let v = previousUserSegments { params["previous_user_segments"] = v }
        if let v = cookieConsents { params["cookie_consents"] = v }
        client.request(
            path: "/api/v3/conversion/logMicroConversion",
            method: PianoAPI.HttpMethod.from("POST"),
            params: params,
            completion: completion
        )
    }
}
