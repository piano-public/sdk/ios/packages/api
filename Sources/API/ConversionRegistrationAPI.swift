import Foundation

@objc(PianoAPIConversionRegistrationAPI)
public class ConversionRegistrationAPI: NSObject {

    /// Create registration term conversion
    /// - Parameters:
    ///   - aid: The application ID
    ///   - termId: The term ID
    ///   - userToken: The user token
    ///   - userProvider: The user token provider
    ///   - userRef: The encrypted user reference
    ///   - tbc: Piano&#39;s browser cookie containing the browser ID which can also be retrieved through JS SDK 
    ///   - pageviewId: The pageview ID, can be retrieved from cookies through JS SDK
    ///   - callback: Operation callback
    @objc public func createRegistrationConversion(
        aid: String,
        termId: String,
        userToken: String? = nil,
        userProvider: String? = nil,
        userRef: String? = nil,
        tbc: String? = nil,
        pageviewId: String? = nil,
        completion: @escaping (TermConversion?, Error?) -> Void) {
        guard let client = PianoAPI.shared.client else {
            completion(nil, PianoAPIError("PianoAPI not initialized"))
            return
        }

        var params = [String:String]()
        params["aid"] = aid
        params["term_id"] = termId
        if let v = userToken { params["user_token"] = v }
        if let v = userProvider { params["user_provider"] = v }
        if let v = userRef { params["user_ref"] = v }
        if let v = tbc { params["tbc"] = v }
        if let v = pageviewId { params["pageview_id"] = v }
        client.request(
            path: "/api/v3/conversion/registration/create",
            method: PianoAPI.HttpMethod.from("GET"),
            params: params,
            completion: completion
        )
    }
}
