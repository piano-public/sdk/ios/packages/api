import Foundation

@objc(PianoAPIConversionExternalAPI)
public class ConversionExternalAPI: NSObject {

    /// Create external conversion (anon)
    /// - Parameters:
    ///   - aid: The application ID
    ///   - termId: The term ID
    ///   - fields: A JSON object defining what fields have to be checked with the external API
    ///   - userToken: The user token
    ///   - userProvider: The user token provider
    ///   - userRef: The encrypted user reference
    ///   - trackingId: The conversion ID to track in external systems
    ///   - customParams: The custom parameters (any key-value pairs) to save; the value should be a valid JSON object)
    ///   - browserId: The unique browser ID
    ///   - pageTitle: Page title
    ///   - url: The URL of the page
    ///   - referer: The page referer
    ///   - contentAuthor: The author of the content
    ///   - contentCreated: When the content was published
    ///   - contentSection: The section for the content
    ///   - contentType: The type of the content
    ///   - tags: The tags of the page
    ///   - previousUserSegments: Previous C1X user segments
    ///   - cookieConsents: The conversion consents in JSON format
    ///   - callback: Operation callback
    @objc public func externalVerifiedCreate(
        aid: String,
        termId: String,
        fields: String,
        userToken: String? = nil,
        userProvider: String? = nil,
        userRef: String? = nil,
        trackingId: String? = nil,
        customParams: String? = nil,
        browserId: String? = nil,
        pageTitle: String? = nil,
        url: String? = nil,
        referer: String? = nil,
        contentAuthor: String? = nil,
        contentCreated: String? = nil,
        contentSection: String? = nil,
        contentType: String? = nil,
        tags: [String]? = nil,
        previousUserSegments: String? = nil,
        cookieConsents: String? = nil,
        completion: @escaping (TermConversion?, Error?) -> Void) {
        guard let client = PianoAPI.shared.client else {
            completion(nil, PianoAPIError("PianoAPI not initialized"))
            return
        }

        var params = [String:String]()
        params["aid"] = aid
        params["term_id"] = termId
        params["fields"] = fields
        if let v = userToken { params["user_token"] = v }
        if let v = userProvider { params["user_provider"] = v }
        if let v = userRef { params["user_ref"] = v }
        if let v = trackingId { params["tracking_id"] = v }
        if let v = customParams { params["custom_params"] = v }
        if let v = browserId { params["browser_id"] = v }
        if let v = pageTitle { params["page_title"] = v }
        if let v = url { params["url"] = v }
        if let v = referer { params["referer"] = v }
        if let v = contentAuthor { params["content_author"] = v }
        if let v = contentCreated { params["content_created"] = v }
        if let v = contentSection { params["content_section"] = v }
        if let v = contentType { params["content_type"] = v }
        if let v = tags { params["tags"] = v.joined(separator: ",") }
        if let v = previousUserSegments { params["previous_user_segments"] = v }
        if let v = cookieConsents { params["cookie_consents"] = v }
        client.request(
            path: "/api/v3/conversion/external/create",
            method: PianoAPI.HttpMethod.from("POST"),
            params: params,
            completion: completion
        )
    }
}
