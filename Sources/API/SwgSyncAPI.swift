import Foundation

@objc(PianoAPISwgSyncAPI)
public class SwgSyncAPI: NSObject {

    /// List user&#39;s external subscriptions by user&#39;s access token (for Piano ID users only)
    /// - Parameters:
    ///   - aid: The application ID
    ///   - accessToken: The access token
    ///   - callback: Operation callback
    @objc public func syncExternal(
        aid: String,
        accessToken: String,
        completion: @escaping (SwgResponse?, Error?) -> Void) {
        guard let client = PianoAPI.shared.client else {
            completion(nil, PianoAPIError("PianoAPI not initialized"))
            return
        }

        var params = [String:String]()
        params["aid"] = aid
        params["access_token"] = accessToken
        client.request(
            path: "/api/v3/swg/sync/external",
            method: PianoAPI.HttpMethod.from("GET"),
            params: params,
            completion: completion
        )
    }
}
