import Foundation

@objc(PianoAPIAnonAssetsAPI)
public class AnonAssetsAPI: NSObject {

    /// Get Google Analytics tracking ID
    /// - Parameters:
    ///   - aid: The application ID
    ///   - callback: Operation callback
    @objc public func gaAccount(
        aid: String,
        completion: @escaping (String?, Error?) -> Void) {
        guard let client = PianoAPI.shared.client else {
            completion(nil, PianoAPIError("PianoAPI not initialized"))
            return
        }

        var params = [String:String]()
        params["aid"] = aid
        client.request(
            path: "/api/v3/anon/assets/gaAccount",
            method: PianoAPI.HttpMethod.from("GET"),
            params: params,
            completion: completion
        )
    }
}
