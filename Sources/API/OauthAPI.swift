import Foundation

@objc(PianoAPIOauthAPI)
public class OauthAPI: NSObject {

    /// Get access token for OAuth
    /// - Parameters:
    ///   - clientId: The client ID of OAuth authorization
    ///   - clientSecret: The client secret of OAuth authorization
    ///   - code: The OAuth code of OAuth authorization
    ///   - refreshToken: The OAuth refresh token of OAuth authorization
    ///   - grantType: The grant type of OAuth authorization
    ///   - redirectUri: The redirect URI of OAuth authorization
    ///   - username: The OAuth username
    ///   - password: The OAuth password
    ///   - state: The OAuth state
    ///   - deviceId: The device ID for OAuth
    ///   - callback: Operation callback
    @objc public func authToken(
        clientId: String,
        clientSecret: String? = nil,
        code: String? = nil,
        refreshToken: String? = nil,
        grantType: String? = nil,
        redirectUri: String? = nil,
        username: String? = nil,
        password: String? = nil,
        state: String? = nil,
        deviceId: String? = nil,
        completion: @escaping (OAuthToken?, Error?) -> Void) {
        guard let client = PianoAPI.shared.client else {
            completion(nil, PianoAPIError("PianoAPI not initialized"))
            return
        }

        var params = [String:String]()
        params["client_id"] = clientId
        if let v = clientSecret { params["client_secret"] = v }
        if let v = code { params["code"] = v }
        if let v = refreshToken { params["refresh_token"] = v }
        if let v = grantType { params["grant_type"] = v }
        if let v = redirectUri { params["redirect_uri"] = v }
        if let v = username { params["username"] = v }
        if let v = password { params["password"] = v }
        if let v = state { params["state"] = v }
        if let v = deviceId { params["device_id"] = v }
        client.request(
            path: "/api/v3/oauth/authToken",
            method: PianoAPI.HttpMethod.from("POST"),
            params: params,
            completion: completion
        )
    }
}
