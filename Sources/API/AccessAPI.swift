import Foundation

@objc(PianoAPIAccessAPI)
public class AccessAPI: NSObject {

    /// Get access details for user and RID
    /// - Parameters:
    ///   - rid: The resource ID
    ///   - aid: The application ID
    ///   - tpAccessTokenV2: Piano&#39;s access token (v2)
    ///   - umc: Piano&#39;s user meter cookie (umc)
    ///   - crossApp: Whether to include cross application access for the resource
    ///   - userToken: The user token
    ///   - userProvider: The user token provider
    ///   - userRef: The encrypted user reference
    ///   - callback: Operation callback
    @objc public func check(
        rid: String,
        aid: String,
        tpAccessTokenV2: String? = nil,
        umc: String? = nil,
        crossApp: OptionalBool = false,
        userToken: String? = nil,
        userProvider: String? = nil,
        userRef: String? = nil,
        completion: @escaping (AccessDTO?, Error?) -> Void) {
        guard let client = PianoAPI.shared.client else {
            completion(nil, PianoAPIError("PianoAPI not initialized"))
            return
        }

        var params = [String:String]()
        params["rid"] = rid
        params["aid"] = aid
        if let v = tpAccessTokenV2 { params["tp_access_token_v2"] = v }
        if let v = umc { params["umc"] = v }
        params["cross_app"] = String(crossApp.value)
        if let v = userToken { params["user_token"] = v }
        if let v = userProvider { params["user_provider"] = v }
        if let v = userRef { params["user_ref"] = v }
        client.request(
            path: "/api/v3/access/check",
            method: PianoAPI.HttpMethod.from("GET"),
            params: params,
            completion: completion
        )
    }

    /// List user&#39;s accesses (anon)
    /// - Parameters:
    ///   - aid: The application ID
    ///   - offset: Offset from which to start returning results
    ///   - limit: Maximum index of returned results
    ///   - active: Whether the object is active
    ///   - expandBundled: Whether to expand bundled accesses in the response
    ///   - crossApp: Whether to include cross application access for the resource
    ///   - userToken: The user token
    ///   - userProvider: The user token provider
    ///   - userRef: The encrypted user reference
    ///   - callback: Operation callback
    @objc public func list(
        aid: String,
        offset: OptionalInt = 0,
        limit: OptionalInt = 100,
        active: OptionalBool = true,
        expandBundled: OptionalBool = false,
        crossApp: OptionalBool = false,
        userToken: String? = nil,
        userProvider: String? = nil,
        userRef: String? = nil,
        completion: @escaping ([AccessDTO]?, Error?) -> Void) {
        guard let client = PianoAPI.shared.client else {
            completion(nil, PianoAPIError("PianoAPI not initialized"))
            return
        }

        var params = [String:String]()
        params["aid"] = aid
        params["offset"] = String(offset.value)
        params["limit"] = String(limit.value)
        params["active"] = String(active.value)
        params["expand_bundled"] = String(expandBundled.value)
        params["cross_app"] = String(crossApp.value)
        if let v = userToken { params["user_token"] = v }
        if let v = userProvider { params["user_provider"] = v }
        if let v = userRef { params["user_ref"] = v }
        client.request(
            path: "/api/v3/access/list",
            method: PianoAPI.HttpMethod.from("GET"),
            params: params,
            completion: completion
        )
    }
}
