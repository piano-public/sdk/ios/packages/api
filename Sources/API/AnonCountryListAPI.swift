import Foundation

@objc(PianoAPIAnonCountryListAPI)
public class AnonCountryListAPI: NSObject {

    /// Get list of countries
    /// - Parameters:
    ///   - callback: Operation callback
    @objc public func listCountry(
        completion: @escaping ([Country]?, Error?) -> Void) {
        guard let client = PianoAPI.shared.client else {
            completion(nil, PianoAPIError("PianoAPI not initialized"))
            return
        }

        
        client.request(
            path: "/api/v3/anon/country/list/country",
            method: PianoAPI.HttpMethod.from("GET"),
            completion: completion
        )
    }
}
