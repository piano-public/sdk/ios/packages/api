import Foundation

@objc(PianoAPIAccessTokenAPI)
public class AccessTokenAPI: NSObject {

    /// List user&#39;s access tokens
    /// - Parameters:
    ///   - aid: The application ID
    ///   - url: The URL of the page
    ///   - userToken: The user token
    ///   - userProvider: The user token provider
    ///   - userRef: The encrypted user reference
    ///   - callback: Operation callback
    @objc public func tokenList(
        aid: String,
        url: String? = nil,
        userToken: String? = nil,
        userProvider: String? = nil,
        userRef: String? = nil,
        completion: @escaping (AccessTokenList?, Error?) -> Void) {
        guard let client = PianoAPI.shared.client else {
            completion(nil, PianoAPIError("PianoAPI not initialized"))
            return
        }

        var params = [String:String]()
        params["aid"] = aid
        if let v = url { params["url"] = v }
        if let v = userToken { params["user_token"] = v }
        if let v = userProvider { params["user_provider"] = v }
        if let v = userRef { params["user_ref"] = v }
        client.request(
            path: "/api/v3/access/token/list",
            method: PianoAPI.HttpMethod.from("GET"),
            params: params,
            completion: completion
        )
    }
}
